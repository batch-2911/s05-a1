@extends('layouts.app')

@section('content')

    <div class="container">
        @if($errors->any())
        @foreach($errors->all() as $error)      
            <div class="alert alert-danger">{{ $error }}</div>
            @endforeach
         @endif

        <div class="card">
            <div class="card-body">
                <h2>Update Post</h2>

                <form action="{{ route('posts.update', $post->id) }}" method="POST">
                    @csrf
                    @method('PUT')

                    <div class="form-group row">
                        <label for="title" class="col-md-4 col-form-label text-md-right">Title</label>
                        <input type="text" name="title" value= "{{ $post->title }}" class="form-control" />
                    </div>

                    <div class="form-group row">
                        <label for="title" class="col-md-4 col-form-label text-md-right">Content</label>
                        <textarea name="content" cols="30" rows="10">{{ $post->content }}</textarea>
                    </div>

                    <div class="form-group row mb-0">
                        <div class="col-md-6 offset-md-4">
                            <button type="submit" class="btn btn-primary">
                                Update
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
                
@endsection