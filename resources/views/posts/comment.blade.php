@if ($post->comments->count() > 0)
<div class="mt-5">
    <h1>Comments</h1>
    @foreach ($post->comments as $comment)
     <div class="card mt-5">
        <h2 class="text-center">{{ $comment->content }}</h2>
        <div class="text-end">
            <h5>Author: {{ $comment->user->name }}</h5>
            <h5>Created_at: {{ $comment->created_at }}</h5>
        </div>
     </div>
     @endforeach
</div>

@endif
        